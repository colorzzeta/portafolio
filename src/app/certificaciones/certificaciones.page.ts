import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

@Component({
  selector: 'app-certificaciones',
  templateUrl: './certificaciones.page.html',
  styleUrls: ['./certificaciones.page.scss'],
  standalone: true,
  imports: [IonicModule, CommonModule, FormsModule],
})
export class CertificacionesPage implements OnInit {
  certificationsList = [
    {
      title: 'Tecnico En Programacion De Software',
      description:
        'Responsabilidad con los proyectos para el desarrollo o innovación de piezas tecnológicas y su respectiva aplicación en programas,  Capacidad de diseñar y producir componentes de software inteligente para ser integrado en aplicaciones funcionales y técnicas,  Capacidad de comprensión de requerimientos de software,  Dominio de lenguajes de programación,  Manejo de herramientas para gestionar requerimientos y ambientes de desarrollo,  Conocimiento en base de datos,  Conocimiento en arquitectura de software,  Servicios web y aplicaciones orientadas a objetos (POO),  Algoritmos y estructuración de datos,  Capacidad de resolución de problemas,  Creatividad,  Poder de autogestión.',
      url:"../../assets/TPStecnicoenprogramaciondesoftware.svg"
    },
    {
      title: 'Curso de MEAN Stack y MERN Stack',
      description:'Aprende JavaScript moderno con ECMAScript, TypeScript, Angular 9+, React, NodeJS, MongoDB (MEAN Stack y MERN Stack)',
        url:"../../assets/certificadoreact.svg"
    },
    {
      title: 'Curso de AWS  ',
      description:
        'Trabajar con el entorno de red VPC Crear y configurar máquinas virtuales con EC2 ,Aprender a crear grupos de escalada y alta disponibilidad ,Gestionar imágenes y plantillas ,Usar bases de datos de todo tipo: RDS, documentales, data warehouse, etcétera ,Aprender a crear y usar migraciones de distinto tipo ,Usar herramientas para desarrolladores. Cloud 9, Codecommit, etc... ,Trabajar en entornos de contenedores y kubernetes, EKS y ECS ,Aprender a monitorizar el entorno con CloudWatch ,Crear y manejar los distintos tipos de almacenamiento de Amazon: S3, EBS,  FXs, etc ,Saber gestionar los costes y la facturación ,Usar Cloud Formation para crear plantillas e infraestructuras',
        url:"../../assets/certAws.svg"
    },
    {
      title: 'Ingles A2-2',
      description:
        'Es capaz de comprender frases y expresiones de uso frecuente relacionadas con áreas de experiencia que le son especialmente relevantes (información básica sobre sí mismo y su familia, compras, lugares de interés, ocupaciones, etc.).Sabe comunicarse a la hora de llevar a cabo tareas simples y cotidianas que no requieran más que intercambios sencillos y directos de información sobre cuestiones que le son conocidas o habituales.Sabe describir en términos sencillos aspectos de su pasado y su entorno así como cuestiones relacionadas con sus necesidades.',
        url:"../../assets/ingles.svg"
    },
    {
      title: 'Mercadeo y ventas',
      description:'Logro desarrollar conceptos como relaciones interpersonales, desarrollo de marca personal, oratoria, gestión de tiempo, desarrollo profesional ',
        url:"../../assets/mercadeoyventas.svg"
    },
  
  ];
  constructor() {}

  ngOnInit() {}
  isModalOpen = false;

  setOpen(isOpen: boolean) {
    this.isModalOpen = isOpen;
  }
  
}
