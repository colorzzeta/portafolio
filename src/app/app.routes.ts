import { Routes } from '@angular/router';

export const routes: Routes = [
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full',
  },



  {
    path: 'home',
    loadComponent: () => import('./home/home.page').then( m => m.HomePage)
  },
  {
    path: 'certificaciones',
    loadComponent: () => import('./certificaciones/certificaciones.page').then( m => m.CertificacionesPage)
  },
  {
    path: 'contacto',
    loadComponent: () => import('./contacto/contacto.page').then( m => m.ContactoPage)
  },
  {
    path: 'acercade',
    loadComponent: () => import('./acercade/acercade.page').then( m => m.AcercadePage)
  },
];
