import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';


@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.page.html',
  styleUrls: ['./contacto.page.scss'],
  standalone: true,
  imports: [IonicModule, CommonModule, FormsModule]
})
export class ContactoPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  redirectToWhatsApp() {
    const message = encodeURIComponent(' ¡Hola, Jesús, vengo de tu página! :D .');
    window.open('https://wa.me/573227623346?text=' + message, '_blank');
  }
  redirectToGmail() {
    const recipient = 'colorzzeta@gmail.com';
    const subject = '¡Hola, Jesús, vengo de tu página! :D ';
    const body = '';
    const url = `https://mail.google.com/mail/?view=cm&to=${recipient}&su=${subject}&body=${body}`;
    window.open(url, 'popup', 'width=600,height=600,scrollbars=yes');
    window.alert('El correo ha sido enviado.');
  }
  redirectToLinkedIn() {
    const username = 'jesus-gabriel-manrique-serrano-73711a204';
    const message = 'Hola, ¿cómo estás?';
    const url = `https://www.linkedin.com/in/${username}/?msg=${message}`;
    window.open(url, '_blank');
  }
}
