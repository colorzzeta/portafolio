import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { RouterLink, RouterLinkActive } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { Router } from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
  standalone: true,
  imports: [IonicModule, RouterLink, RouterLinkActive, CommonModule],
})
export class AppComponent {
  public appPages = [

    { title: 'Home', url: '/home', icon: 'home' },
    { title: 'Certificaciones', url: '/certificaciones', icon: 'home' },
    { title: 'Contacto', url: '/contacto', icon: 'chatbubbles' },
    { title: 'Acerca de', url: '/acercade', icon: 'information-circle' },
  ];
  public labels = [];
  constructor( private router: Router,) {}
  goToContact() {
    this.router.navigate(['/contacto']);
  }
}
