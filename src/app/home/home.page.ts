import { Component, OnInit, Renderer2, ElementRef } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
  standalone: true,
  imports: [IonicModule, CommonModule, FormsModule],
})
export class HomePage implements OnInit {
  skillsArray = [
    { skill: 'PHP', percentage: 70 },
    { skill: 'JAVASCRIPT', percentage: 75 },
    { skill: 'LARAVEL', percentage: 75 },
    { skill: 'REACT JS', percentage: 60 },
    { skill: 'ANGULAR JS', percentage: 70 },
    { skill: 'IONIC FRAMEWORK', percentage: 65 },
    { skill: 'NEST JS', percentage: 60 },
    { skill: 'EXPRESS JS', percentage: 60 },
    { skill: 'AWS', percentage: 45 },
    { skill: 'MONGODB', percentage: 50 },
    { skill: 'MYSQL', percentage: 75 },
    { skill: 'GIT', percentage: 70 },
  ];
  services = [
    {
      title: 'Diseño y estructuracion en bases de datos ',
      description:
        'Cuento con la capacidad de estructurar bases de datos relacionales y no relacionales,realizar consultas y procedimientos almacenados entre otras funciones',
    },
    {
      title: 'Creacion e implementacion de paginas web',
      description:
        'Tengo habilidades en el desarrollo de páginas web utilizando una variedad de frameworks y tecnologías, lo que me permite crear soluciones personalizadas y adaptadas a las necesidades específicas de cada cliente. Algunos de los frameworks con los que estoy más familiarizado incluyen Angular, React,Laravel y Ionic, entre otros. ',
    },
    {
      title: 'Creacion e implementacion de webservices',
      description:
        'Tengo amplio conocimiento y experiencia en la creación de web services, tanto utilizando PHP nativo como también frameworks como Laravel, NestJS y Express. He trabajado en diversos proyectos donde he desarrollado y mantenido web services eficientes y escalables para satisfacer las necesidades de los usuarios. ',
    },
    {
      title: 'Manejo de Git',
      description:
        'Poseo experiencia en el uso de Git como herramienta de control de versiones, incluyendo la implementación de integración continua y el manejo del marco Gitflow. Esto me permite garantizar un desarrollo colaborativo y ordenado en los proyectos en los que participo',
    },
    {
      title: 'Manejo de AWS',
      description:
        'Tengo conocimiento en el uso de diferentes servicios en la nube de Amazon Web Services (AWS), como Amazon EC2, Amazon S3 y Amazon Lambda',
    },
  ];
  indice = 0;

  constructor(
    private router: Router,
    private renderer: Renderer2,
    private elementRef: ElementRef
  ) {}

  ngOnInit() {

    setInterval(() => {
      this.changeText();
    }, 2500);
  }

  goToCertifications() {
    this.router.navigate(['/certificaciones']);
  }
  goToContact() {
    this.router.navigate(['/contacto']);
  }
  goToAbout() {
    this.router.navigate(['/acercade']);
  }
  changeText() {
    // Accede al elemento utilizando Renderer2
    const textoElemento = this.renderer.selectRootElement('#texto');

    // Actualiza el contenido del elemento con la habilidad  actual
    this.renderer.setProperty(
      textoElemento,
      'textContent',
      `${this.skillsArray[this.indice].skill}`
    );

    // Incrementa el índice para obtener la siguiente habilidad y porcentaje del array
    this.indice++;

    // Si el índice es mayor o igual al número de elementos del array, reinícialo a cero
    if (this.indice >= this.skillsArray.length) {
      this.indice = 0;
    }
  }

}
